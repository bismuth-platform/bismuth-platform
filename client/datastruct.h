#ifndef __DATASTRUCT_H__
#define __DATASTRUCT_H__
typedef struct{
	void* next;
	void* back;
	void* data;
	long long data_type_t;
} node;
typedef struct{
	long long length;
	void* first;
	void* last;
} list;
typedef struct{
	void* data;
	long long data_type;
} data_tuple;
typedef struct{
	list key;
	list value;
} dict;
data_tuple list_get(long long);
data_tuple dict_get(data_tuple);
#endif
